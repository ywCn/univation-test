import React from "react"

function SvgComponent(props) {
  return (
    <svg
      id="prefix__icons8-facebook_1_"
      width={18}
      height={18}
      data-name="icons8-facebook (1)"
      viewBox="0 0 21.626 21.629"
      cursor="pointer"
      {...props}
    >
      <defs>
        <style />
      </defs>
      <path
        id="prefix__Subtraction_1"
        d="M18.623 21.629H3a3.007 3.007 0 01-3-3V3a3.007 3.007 0 013-3h15.62a3.007 3.007 0 013 3v15.625a3.007 3.007 0 01-2.997 3.004zm-6.608-10.215v7.81h3v-7.81h2.022l.38-2.4h-2.4V7.645c0-.663.048-1.037 1.03-1.037h1.373v-2.4h-2.045a3.36 3.36 0 00-2.609.889 3.515 3.515 0 00-.75 2.469v1.447h-1.8v2.4z"
        data-name="Subtraction 1"
      />
    </svg>
  )
}

export default SvgComponent
