import React from "react"

function SvgComponent(props) {
  return (
    <svg
      width={18}
      height={18}
      viewBox="0 0 21.627 21.627"
      cursor="pointer"
      {...props}
    >
      <defs>
        <style />
      </defs>
      <path
        id="prefix__Subtraction_6"
        d="M10.816 21.627A10.818 10.818 0 010 10.812a10.814 10.814 0 0118.46-7.645 10.812 10.812 0 010 15.291 10.73 10.73 0 01-7.644 3.169zm3.23-14.6a.674.674 0 00-.236.044c-1.769.729-3.61 1.363-5.381 1.972-.7.241-1.38.476-2.062.717a.639.639 0 00-.419.595.623.623 0 00.435.6l2.9 1.48 1.529 3.122a.632.632 0 001.162-.067l2.668-7.62a.575.575 0 00.037-.21.632.632 0 00-.633-.63z"
        data-name="Subtraction 6"
      />
    </svg>
  )
}

export default SvgComponent
