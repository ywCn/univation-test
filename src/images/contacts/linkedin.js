import React from "react"

function SvgComponent(props) {
  return (
    <svg
      id="prefix__icons8-linkedin_1_"
      width={18}
      height={18}
      data-name="icons8-linkedin (1)"
      viewBox="0 0 21.628 21.628"
      cursor="pointer"
      {...props}
    >
      <defs>
        <style />
      </defs>
      <path
        id="prefix__Subtraction_2"
        d="M18.624 21.628H3a3.007 3.007 0 01-3-3V3a3.007 3.007 0 013-3h15.62a3.007 3.007 0 013 3v15.62a3.007 3.007 0 01-2.996 3.008zM13.1 10.336a1.954 1.954 0 011.919 2.222v5.466h3V12.18a4.686 4.686 0 00-1.075-3.316 3.57 3.57 0 00-2.687-1.053 3.032 3.032 0 00-2.846 1.572V7.811h-3v10.213h3V12.437a3.178 3.178 0 01.062-.906 1.742 1.742 0 011.627-1.195zM3.6 7.811v10.213h3V7.811zM5.117 3.6A1.443 1.443 0 003.6 5.106a1.441 1.441 0 001.476 1.5H5.1a1.442 1.442 0 001.511-1.5A1.436 1.436 0 005.117 3.6z"
        data-name="Subtraction 2"
      />
    </svg>
  )
}

export default SvgComponent