import React from "react"

function SvgComponent(props) {
  return (
    <svg
      id="prefix__icons8-instagram"
      width={18}
      height={18}
      viewBox="0 0 21.627 21.627"
      cursor="pointer"
      {...props}
    >
      <defs>
        <style />
      </defs>
      <path
        id="prefix__Path_17"
        d="M6.278 0A6.29 6.29 0 000 6.278v9.072a6.29 6.29 0 006.278 6.278h9.072a6.29 6.29 0 006.278-6.278V6.278A6.29 6.29 0 0015.35 0zm0 1.664h9.072a4.6 4.6 0 014.614 4.614v9.072a4.6 4.6 0 01-4.614 4.614H6.278a4.6 4.6 0 01-4.614-4.614V6.278a4.6 4.6 0 014.614-4.614zm10.774 1.663A1.248 1.248 0 1018.3 4.575a1.248 1.248 0 00-1.248-1.248zm-6.238 1.664a5.823 5.823 0 105.823 5.823 5.834 5.834 0 00-5.823-5.823zm0 1.664a4.159 4.159 0 11-4.159 4.159 4.146 4.146 0 014.159-4.159z"
        data-name="Path 17"
      />
    </svg>
  )
}

export default SvgComponent