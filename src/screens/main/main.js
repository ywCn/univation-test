import React, { useState } from 'react';
import { List } from '../../components';
import { FiPlus } from 'react-icons/fi';
import { Affix, Button } from 'antd';



const Main = (props) => {
    const [Addmodal, changeAddmodal] = useState(false);

    return (
        <div >
            <List
                Addmodal={Addmodal}
                changeAddmodal={changeAddmodal}
            />

            <Affix
                style={{ position: 'absolute', right: 30 }}
                offsetBottom={30} >
                <div
                    onClick={() => changeAddmodal(true)}
                    style={{
                        background: '#333', borderRadius: '50%',
                        height: 80, width: 80, justifyContent: 'center', alignItems: 'center', display: 'flex'
                        , color: 'white', cursor: 'pointer'
                    }}>
                    <FiPlus style={{ fontSize: 30 }} />
                </div>
            </Affix>
        </div>
    )
}


export default Main;