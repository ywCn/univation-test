import React, { useState, useEffect } from 'react';
import Row from '../row';
import './list.css';
import Modal from 'react-modal';
import { FiXCircle } from 'react-icons/fi';
import { useTransition, animated } from "react-spring";


const customStyles = {

    content: {
        top: '40%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    },
    overlay: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        zIndex: 1000000,
    }
};




const List = (props) => {

    const [Speak, changeSpeak] = useState([]);
    const [name, changeName] = useState('');
    const [text, changeText] = useState('');
    const [err, changeErr] = useState(false);

    const transitions = useTransition(
        Speak.map((data, i) => ({ ...data, x: i * 20 })),
        d => d.name,
        {
            from: { opacity: 0 },
            leave: { height: 0, opacity: 0 },
            enter: ({ x }) => ({ x, opacity: 1 }),
            update: ({ x }) => ({ x })
        }
    );


    useEffect(() => {
        changeSpeak(JSON.parse(sessionStorage.getItem('list')) ? JSON.parse(sessionStorage.getItem('list')) : []);
    }, [])


    const handleAddItem = async () => {
        let array = Speak ? Speak : [];
        await array.push({ name: name, text: text });
        changeSpeak([...array]);
        await sessionStorage.setItem('list', JSON.stringify(array));
        CloseModal();
    }

    const handleRemoveItem = async (id) => {
        let array = Speak.filter((e, i) => (i != id));
        await changeSpeak(array)
        await sessionStorage.setItem('list', JSON.stringify(array));
        console.log("remove", Speak)
    };

    const CloseModal = () => {
        changeName('');
        changeText('');
        props.changeAddmodal(false);
        changeErr(false);
    }



    return (
        <div className='list-container'>


            {transitions.map(({ item, props: { x, ...rest }, key }, index) => (
                <animated.div
                    key={key}
                    class="Row"
                    style={{
                        transform: x.interpolate(x => `translate3d(0,0,0)`),
                        ...rest
                    }}
                >
                    <Row
                        index={index}
                        item={item}
                        handleRemoveItem={handleRemoveItem}
                    />

                </animated.div>
            ))}

            {Speak && Speak.length === 0 &&
                <div style={{
                    height: '100vh', width: '100%', display: 'flex', fontSize: 30,
                    justifyContent: 'center', alignItems: 'center', overflow: 'hidden'
                }}>
                    empty
                </div>

            }


            <Modal
                isOpen={props.Addmodal}
                onRequestClose={() => props.changeAddmodal(false)}
                style={customStyles}
            >
                <div style={{ padding: 0, width: 550, paddingTop: 20 }}>
                    <div className='modal-header'>
                        Add Item
                        <FiXCircle
                            onClick={() => CloseModal()}
                            style={{ position: 'absolute', right: 30, top: 0, fontSize: 25, cursor: 'pointer' }}
                        />

                    </div>

                    <div className='modal-container'>
                        <div>name:</div>
                        <input
                            style={{ border: (err && !name ? 'solid 1px red' : 'solid 1px #333') }}
                            onChange={(e) => changeName(e.target.value)}
                            className={'input'}
                        />
                        <div style={{ marginTop: 15 }}>text:</div>
                        <textarea
                            style={{ border: (err && !text ? 'solid 1px red' : 'solid 1px #333') }}
                            onChange={(e) => changeText(e.target.value)}
                            className={'textarea'}
                        />
                    </div>

                    <div style={{
                        flexDirection: 'row', display: 'flex', height: 40,
                        justifyContent: 'flex-end', alignItems: 'center',
                        marginRight: 15, marginBottom: 20
                    }}>
                        <div
                            onClick={() => CloseModal()}
                            className='cansel'>
                            Cansel
                        </div>
                        <div
                            onClick={() => { (name && text) ? handleAddItem() : changeErr(true) }}
                            className='add'>
                            Add
                        </div>
                    </div>
                </div>


            </Modal>

        </div>
    );
}


export default List;