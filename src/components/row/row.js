import React from 'react';
import './row.css'
import { Facebook, Linkedin, Twitter } from '../../images/contacts';
import { FiXCircle } from 'react-icons/fi';

const Row = (props) => {
    return (
        <>
            <FiXCircle
                onClick={() => props.handleRemoveItem(props.index)}
                style={{ position: 'absolute', right: 10, top: 10, fontSize: 25, cursor: 'pointer' }}
            />

            <div className='Row-Container'>
                <div className='Row-Container-text'>{props.item.text}</div>
                <div className='Row-Container-title'>{props.item.name}</div>
            </div>

            <div
                className='Row-footer'>
                <Linkedin />
                <Facebook />
                <Twitter />
            </div>
        </>
    );
}

export default Row;