import React from 'react';
import './header.css'



const Header = (props) => {
    return (
        <div className='header'>
            <div className='header-title'>
                Univation
            </div>

            <div className='menu-list'>
                <div className='menu-list-active'>
                    mainPage
                </div>
                <div>
                    About Us
                </div>
                <div>
                    Contact Us
                </div>

            </div>

        </div>
    );
}

export default Header;