import React from 'react';
import './App.css';
import { MainPage } from './screens';
import { Header } from './components';
import 'bootstrap/dist/css/bootstrap.css';
import 'antd/dist/antd.css';

const App = (props) => {
  return (
    <div style={{height:'100vh'}}>
      <Header />
      <MainPage />

    </div>
  )
}



export default App;
